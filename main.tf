module "transit" {
  for_each = var.transit
  source   = "terraform-aviatrix-modules/mc-transit/aviatrix"
  version  = "2.0.0"

  name                   = each.value.name
  cloud                  = each.value.cloud
  ha_gw                  = local.transit[each.key].ha_gw
  cidr                   = each.value.transit_cidr
  region                 = each.value.region
  local_as_number        = each.value.asn
  account                = lookup(local.account, each.value.cloud, null) #aviatrix_account.aws_connectivity.account_name
  enable_segmentation    = local.transit[each.key].segmentation
  enable_transit_firenet = local.transit[each.key].firenet
}

module "firenet" {
  for_each = { for k, v in module.transit : k => module.transit[k] if local.transit[k].firenet } #Filter transits that have firenet enabled
  source   = "terraform-aviatrix-modules/mc-firenet/aviatrix"
  version  = "1.0.0"

  transit_module = module.transit[each.key]
  firewall_image = lookup(local.firewall_image, local.transit[each.key].cloud, null)
}

#Create full mesh peerings
module "transit-peerings" {
  source  = "terraform-aviatrix-modules/mc-transit-peering/aviatrix"
  version = "1.0.5"

  transit_gateways = values(module.transit)[*].transit_gateway.gw_name
  excluded_cidrs   = ["0.0.0.0/0", ]
}