locals {
  transit_map = zipmap(
    values(module.transit)[*].vpc.region,
    values(module.transit)[*].transit_gateway.gw_name,
  )
}